variable "env" {
  type = string
}
variable "create_message_build_path" {
  type        = string
  description = "Path of zipped code for create function."
}
variable "find_messages_build_path" {
  type        = string
  description = "Path of zipped code for find function."
}
