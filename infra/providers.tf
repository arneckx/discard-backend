provider "aws" {

  default_tags {
    tags = {
      Owner       = "arneckx"
      Project     = "discard"
      Team        = "javascript"
      Terraform   = "discard-backend/infra"
      Environment = var.env
    }
  }
}
