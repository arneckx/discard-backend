resource "aws_dynamodb_table" "messages_dynamodb_table" {
  name         = "messages"
  billing_mode = "PAY_PER_REQUEST"

  hash_key  = "id"
  range_key = "timestamp"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "stream"
    type = "S"
  }

  attribute {
    name = "timestamp"
    type = "S"
  }

  global_secondary_index {
    name            = "vlot"
    hash_key        = "stream"
    projection_type = "ALL"
    range_key       = "timestamp"
  }
}
