output "messages_api" {
  value       = "${aws_api_gateway_deployment.this.invoke_url}api${aws_api_gateway_resource.messages.path}"
}