data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

resource "aws_api_gateway_rest_api" "discard_api" {
  name = "discard_api"
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.discard_api.id

  triggers = {
    redeployment = sha1(join(",", concat(
      [
        jsonencode(aws_api_gateway_resource.messages)
      ]
    )))
  }

  depends_on = [
    aws_api_gateway_integration.create_message,
    aws_api_gateway_integration.find_messages
  ]
}

resource "aws_api_gateway_method" "get_messages" {
  rest_api_id   = aws_api_gateway_rest_api.discard_api.id
  resource_id   = aws_api_gateway_resource.messages.id
  http_method   = "GET"
  authorization = "NONE"

  request_parameters = {
    "method.request.path.proxy" = true
  }
}

resource "aws_api_gateway_method" "post_message" {
  rest_api_id   = aws_api_gateway_rest_api.discard_api.id
  resource_id   = aws_api_gateway_resource.messages.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_method" "options_message" {
  rest_api_id   = aws_api_gateway_rest_api.discard_api.id
  resource_id   = aws_api_gateway_resource.messages.id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "options_message" {
  rest_api_id             = aws_api_gateway_rest_api.discard_api.id
  resource_id             = aws_api_gateway_resource.messages.id
  http_method             = aws_api_gateway_method.options_message.http_method
  integration_http_method = "OPTIONS"
  type                    = "MOCK"

  request_templates = {
    "application/json" = jsonencode(
      {
        statusCode = 200
      }
    )
  }
}

resource "aws_api_gateway_integration_response" "options_response_200" {
  rest_api_id = aws_api_gateway_rest_api.discard_api.id
  resource_id = aws_api_gateway_resource.messages.id
  http_method = aws_api_gateway_method.options_message.http_method
  status_code = 200

  response_parameters = {
     "method.response.header.Access-Control-Allow-Origin" = "'*'"
     "method.response.header.Access-Control-Allow-Methods" = "'GET,POST,OPTIONS'"
     "method.response.header.Access-Control-Allow-Headers" = "'content-type'"
  }

  depends_on = [aws_api_gateway_integration.options_message]
}

resource "aws_api_gateway_method_response" "options_response_200" {
  rest_api_id = aws_api_gateway_rest_api.discard_api.id
  resource_id = aws_api_gateway_resource.messages.id
  http_method = aws_api_gateway_method.options_message.http_method
  status_code = 200

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Headers" = true,
  }
  response_models     = { "application/json" = "Empty" }
}

resource "aws_api_gateway_integration" "create_message" {
  rest_api_id             = aws_api_gateway_rest_api.discard_api.id
  resource_id             = aws_api_gateway_resource.messages.id
  http_method             = aws_api_gateway_method.post_message.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.create_message.invoke_arn
}

resource "aws_api_gateway_integration" "find_messages" {
  rest_api_id             = aws_api_gateway_rest_api.discard_api.id
  resource_id             = aws_api_gateway_resource.messages.id
  http_method             = aws_api_gateway_method.get_messages.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.find_messages.invoke_arn
}

resource "aws_api_gateway_stage" "api" {
  deployment_id = aws_api_gateway_deployment.this.id
  rest_api_id   = aws_api_gateway_rest_api.discard_api.id
  stage_name    = "api"
}

resource "aws_api_gateway_resource" "messages" {
  rest_api_id = aws_api_gateway_rest_api.discard_api.id
  parent_id   = aws_api_gateway_rest_api.discard_api.root_resource_id
  path_part   = "messages"
}

resource "aws_iam_role_policy_attachment" "apigateway_cloudwatch" {
  role       = aws_iam_role.iam_for_api_gateway.id
  policy_arn = data.aws_iam_policy.api_gateway_cloudwatch.arn
}

resource "aws_api_gateway_account" "events_apigateway" {
  cloudwatch_role_arn = aws_iam_role.iam_for_api_gateway.arn
}

resource "aws_cloudwatch_log_group" "this" {
  name = "API-Gateway-Execution-Logs_${aws_api_gateway_rest_api.discard_api.id}/${aws_api_gateway_stage.api.stage_name}"
}

resource "aws_iam_role" "iam_for_api_gateway" {
  name = "iam_for_api_gateway"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "aws_iam_policy" "api_gateway_cloudwatch" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
}

resource "aws_lambda_permission" "apigw_lambda_get_messages" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.find_messages.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.discard_api.id}/*/${aws_api_gateway_method.get_messages.http_method}${aws_api_gateway_resource.messages.path}"
}

resource "aws_lambda_permission" "apigw_lambda_create_messages" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.create_message.function_name
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.discard_api.id}/*/${aws_api_gateway_method.post_message.http_method}${aws_api_gateway_resource.messages.path}"
}