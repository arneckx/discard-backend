import json

import boto3
from boto3.dynamodb.conditions import Key

def by(event, context):
    after_timestamp = event['queryStringParameters']['afterTimestamp']
    print(f'## TIMESTAMP AFTER -> {after_timestamp}')

    dynamodb = boto3.resource('dynamodb')
    # dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:4566")

    table = dynamodb.Table('messages')

    db_response = table.query(
        IndexName='vlot',
        KeyConditionExpression=Key('stream').eq('vlot') & Key('timestamp').gt(after_timestamp),
        ScanIndexForward=True,
    )

    items = db_response["Items"]
    if len(items) > 0:
        messages = []
        for item in items:
            messages.append(message_of(item))
        response = {
            "statusCode": 200,
            "body": json.dumps(messages),
            "headers": {'content-type': 'application/json', 'Access-Control-Allow-Origin': '*'}
        }
    else:
        print("no messages found")
        response = {
            "statusCode": 200,
            "body": json.dumps([]),
            "headers": {'content-type': 'application/json', 'Access-Control-Allow-Origin': '*'}
        }

    return response


def message_of(item):
    return {"message": item["message"], "nickname": item["nickname"], "avatarImage": item["avatarImage"], "timestamp": item["timestamp"]}
