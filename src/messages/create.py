import json
import boto3
from datetime import datetime
import uuid


def apply(event, context):
    body = json.loads(event['body'])
    print(f'## BODY {body}')

    dynamodb = boto3.resource('dynamodb')
    # dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:4566")

    table = dynamodb.Table('messages')
    event_id = str(uuid.uuid4())
    table.put_item(
        Item={
            'id': event_id,
            'stream': 'vlot',
            'message': body['message'],
            'nickname': body['nickname'],
            'avatarImage': body['avatarImage'],
            'timestamp': datetime.now().isoformat()
        }
    )

    print(f'## CREATED message with id: {event_id}')

    response = {
        "statusCode": 201,
        "body": event_id,
        "headers": {'Access-Control-Allow-Origin': '*'}
    }

    return response
